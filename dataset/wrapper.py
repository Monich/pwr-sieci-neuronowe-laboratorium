import mnist_loader
import numpy

training_data = mnist_loader.load_data();
data = numpy.concatenate((training_data[0][0], training_data[1][0]))
data = numpy.concatenate((data, training_data[2][0]))

answer = numpy.concatenate((training_data[0][1], training_data[1][1]))
answer = numpy.concatenate((answer, training_data[2][1]))

#csv_file = open('data.csv', 'a')
numpy.savetxt("data.csv", data, delimiter=";")
numpy.savetxt("answer.csv", answer, delimiter=";")
