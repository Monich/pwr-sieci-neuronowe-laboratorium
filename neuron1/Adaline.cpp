#include "Adaline.hpp"

#include <iostream>

Adaline::Adaline(double learningRate,
                 double minWeightValue,
                 double maxWeightValue,
                 ActivationFunction& activationFunction) :
    Neuron(learningRate, activationFunction)
{
    std::uniform_real_distribution<double> unif(minWeightValue, maxWeightValue);
    weights[0] = unif(randomEngine);
    weights[1] = unif(randomEngine);
    bias = unif(randomEngine);
}

void Adaline::learn(const NeuronData &data)
{
    double error = data.expectedOutput - calculateLevel(data.x, data.y);

    weights[0] += error * data.x * learningRate * 2;
    weights[1] += error * data.y * learningRate * 2;
    bias += error * learningRate * 2;
}

void Adaline::showWeights()
{
    std::cout << "Weights in Adaline are: {" << weights[0] << ", " << weights[1] << ", bias = " << bias << "}" << std::endl;
}

double Adaline::calculateLevel(double x, double y) const
{
    return weights[0] * x + weights[1] * y + bias;
}

