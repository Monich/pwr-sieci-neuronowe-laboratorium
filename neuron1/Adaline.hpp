#pragma once

#include "Neuron.hpp"

class Adaline : public Neuron
{
public:
    Adaline(double learningRate,
            double minWeightValue,
            double maxWeightValue,
            ActivationFunction& activationFunction);

    void learn(const NeuronData& data) override;
    void showWeights() override;

private:
    double calculateLevel(double x, double y) const override;

    double weights[2]{};
    double bias;
};
