#include "Neuron.hpp"

#include <chrono>

std::default_random_engine randomEngine{static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count())};

std::ostream &operator<<(std::ostream &str, const NeuronData &data)
{
    str << "(" << data.x << ", " << data.y << " -> " << data.expectedOutput << ")";
    return str;
}

double activationUnipolar(double level) {
    return level > 0.5 ? 1 : 0;
}

double activationBipolar(double level) {
    return level > 0.5 ? 1 : -1;
}

double activationBipolarAdaline(double level) {
    return level > 0 ? 1 : -1;
}

Neuron::Neuron(double learningRate, ActivationFunction &activationFunction)
    : learningRate(learningRate),
      activationFunction(activationFunction)
{}

double Neuron::calculate(double x, double y) const {
    return activationFunction(calculateLevel(x, y));
}
