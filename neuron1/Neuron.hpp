#pragma once

#include <functional>
#include <ostream>
#include <random>

struct NeuronData
{
    double x;
    double y;
    double expectedOutput;
};

std::ostream& operator<<(std::ostream& str, const NeuronData& data);

using ActivationFunction = double(double);

extern std::default_random_engine randomEngine;

double activationUnipolar(double);
double activationBipolar(double);
double activationBipolarAdaline(double);

class Neuron
{
public:
    explicit Neuron(double learningRate, ActivationFunction &activationFunction);

    virtual void learn(const NeuronData& data) = 0;

    double calculate(double x, double y) const;
    virtual void showWeights() = 0;

    virtual double calculateLevel(double x, double y) const = 0;

protected:
    double learningRate;

private:
    ActivationFunction& activationFunction;
};
