#pragma once

#include "Neuron.hpp"
#include "Tester.hpp"

class NewAdalineFactory : public NewNeuronFactory
{
public:

    NewAdalineFactory(double learningRate,
                         double minWeightValue,
                         double maxWeightValue,
                         ActivationFunction &activationFunction);

    std::unique_ptr<Neuron> createNeuron() override;

    static std::vector<TestSuite> createFactories(
        const std::vector<std::string> &testSuiteNames,
        double learningRate,
        const std::vector<double>& minWeightValues,
        const std::vector<double>& maxWeightValues,
        ActivationFunction &activationFunction
    );

    static std::vector<TestSuite> createFactories(
        const std::vector<std::string> &testSuiteNames,
        const std::vector<double>& learningRates,
        double minWeightValue,
        double maxWeightValue,
        ActivationFunction &activationFunction
    );

private:
    double learningRate;
    double minWeightValue;
    double maxWeightValue;
    ActivationFunction& activationFunction;
};