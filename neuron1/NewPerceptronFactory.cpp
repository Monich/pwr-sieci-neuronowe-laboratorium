#include <iostream>
#include "NewPerceptronFactory.hpp"

#include "Perceptron.hpp"

NewPerceptronFactory::NewPerceptronFactory(double learningRate, double minWeightValue, double maxWeightValue,
                                           ActivationFunction& activationFunction) : learningRate(learningRate),
                                                                                    minWeightValue(minWeightValue),
                                                                                    maxWeightValue(maxWeightValue),
                                                                                    activationFunction(activationFunction) {}

std::unique_ptr<Neuron> NewPerceptronFactory::createNeuron()
{
    return std::make_unique<Perceptron>(
        learningRate,
        minWeightValue,
        maxWeightValue,
        activationFunction
    );
}

std::vector<TestSuite>
NewPerceptronFactory::createFactories(const std::vector<std::string>& testSuiteNames,
                                      double learningRate,
                                      const std::vector<double>& minWeightValues,
                                      const std::vector<double>& maxWeightValues,
                                      ActivationFunction &activationFunction)
{
    std::vector<TestSuite> factories;

    if(testSuiteNames.size() != minWeightValues.size() || testSuiteNames.size() != maxWeightValues.size())
    {
        throw std::runtime_error("Failed to match vector sizes in create factories call");
    }

    for (unsigned i = 0; i < testSuiteNames.size(); ++i)
    {
        factories.push_back({
            testSuiteNames[i],
            std::make_unique<NewPerceptronFactory>(
                learningRate,
                minWeightValues[i],
                maxWeightValues[i],
                activationFunction
            )
        });
    }

    return factories;
}

std::vector<TestSuite> NewPerceptronFactory::createFactories(const std::vector<std::string> &testSuiteNames,
                                                             const std::vector<double> &learningRates,
                                                             double minWeightValue, double maxWeightValue,
                                                             ActivationFunction &activationFunction) {
    std::vector<TestSuite> factories;

    if(testSuiteNames.size() != learningRates.size())
    {
        throw std::runtime_error("Failed to match vector sizes in create factories call");
    }

    for (unsigned i = 0; i < testSuiteNames.size(); ++i)
    {
        factories.push_back({
                                    testSuiteNames[i],
                                    std::make_unique<NewPerceptronFactory>(
                                            learningRates[i],
                                            minWeightValue,
                                            maxWeightValue,
                                            activationFunction
                                    )
                            });
    }

    return factories;
}
