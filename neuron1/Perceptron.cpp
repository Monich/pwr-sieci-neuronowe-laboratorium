#include <iostream>
#include "Perceptron.hpp"

Perceptron::Perceptron(double learningRate,
                       double minWeightValue,
                       double maxWeightValue,
                       ActivationFunction &activationFunction) :
    Neuron(learningRate, activationFunction)
{
    std::uniform_real_distribution<double> unif(minWeightValue, maxWeightValue);
    weights[0] = unif(randomEngine);
    weights[1] = unif(randomEngine);
}

void Perceptron::learn(const NeuronData &data)
{
    double output = calculate(data.x, data.y);
    double error = data.expectedOutput == output ? 0 : data.expectedOutput > output ? 1 : -1;

    weights[0] += error * data.x * learningRate;
    weights[1] += error * data.y * learningRate;
}

double Perceptron::calculateLevel(double x, double y) const
{
    return weights[0] * x + weights[1] * y;
}

void Perceptron::showWeights()
{
    std::cout << "Weights in Perceptron are: {" << weights[0] << ", " << weights[1] << "}" << std::endl;
}
