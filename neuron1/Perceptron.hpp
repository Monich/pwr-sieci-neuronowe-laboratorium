#pragma once

#include "Neuron.hpp"

class Perceptron : public Neuron
{
public:
    Perceptron(double learningRate,
               double minWeightValue,
               double maxWeightValue,
               ActivationFunction& activationFunction);

    void learn(const NeuronData &data) override;

    void showWeights() override;

protected:
    double calculateLevel(double x, double y) const override;

private:
    double weights[2]{};
};
