#include "StrictExitClause.hpp"

#include "Neuron.hpp"

bool StrictExitClause::shouldExit(std::unique_ptr<Neuron> &ptr, std::vector<NeuronData> &vector)
{
    for (const auto &data : vector)
    {
        if(ptr->calculate(data.x, data.y) != data.expectedOutput)
        {
            return false;
        }
    }

    return true;
}

MeanErrorExitClause::MeanErrorExitClause(double acceptanceError)
    : acceptanceError(acceptanceError)
{}

bool MeanErrorExitClause::shouldExit(std::unique_ptr<Neuron> &ptr, std::vector<NeuronData> &vector)
{
    double errors[]{
            vector[0].expectedOutput - ptr->calculateLevel(vector[0].x, vector[0].y),
            vector[1].expectedOutput - ptr->calculateLevel(vector[1].x, vector[1].y),
            vector[2].expectedOutput - ptr->calculateLevel(vector[2].x, vector[2].y),
            vector[3].expectedOutput - ptr->calculateLevel(vector[3].x, vector[3].y),
    };

    double meanError = (std::pow(errors[0], 2) +
                 std::pow(errors[1], 2) +
                 std::pow(errors[2], 2) +
                 std::pow(errors[3], 2)) / 4;

    return meanError < acceptanceError;
}
