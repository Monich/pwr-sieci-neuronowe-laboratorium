#pragma once

#include "Tester.hpp"

class StrictExitClause : public TrainingExitClause
{
public:
    bool shouldExit(std::unique_ptr<Neuron> &ptr, std::vector<NeuronData> &vector) override;
};

class MeanErrorExitClause : public TrainingExitClause
{
public:
    explicit MeanErrorExitClause(double acceptanceError);
    bool shouldExit(std::unique_ptr<Neuron> &ptr, std::vector<NeuronData> &vector) override;
private:
    double acceptanceError;
};
