#include "Tester.hpp"

#include "Trainer.hpp"

#include <iostream>

void Tester::performTest(const std::string& testName,
                         const std::vector<TestSuite> &testSuites,
                         TrainingExitClause &trainingExitClause,
                         std::vector<NeuronData>& trainingData,
                         unsigned repetitionTimes)
{
    std::cout << "Performing test " << testName << std::endl;

    for (const auto &testSuite : testSuites)
    {
        u64 iterationsTotal{0};

        for (unsigned i = 0; i < repetitionTimes; ++i)
        {
            Trainer trainer{
                *testSuite.second,
                trainingExitClause
            };
            iterationsTotal += trainer.train(trainingData);
        }

        std::cout << testSuite.first << ": " << iterationsTotal/repetitionTimes << std::endl;
    }

    std::cout << std::endl;
}
