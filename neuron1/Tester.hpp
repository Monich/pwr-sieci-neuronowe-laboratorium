#pragma once

#include <memory>
#include <vector>
#include <string>

class Neuron;
struct NeuronData;

class NewNeuronFactory
{
public:
    virtual std::unique_ptr<Neuron> createNeuron() = 0;
};

class TrainingExitClause
{
public:
    virtual bool shouldExit(std::unique_ptr<Neuron>&, std::vector<NeuronData>&) = 0;
};

using TestSuite = std::pair<std::string, std::unique_ptr<NewNeuronFactory>>;

class Tester
{
public:
    static void performTest(
        const std::string& testName,
        const std::vector<TestSuite> &testSuites,
        TrainingExitClause &trainingExitClause,
        std::vector<NeuronData>& trainingData,
        unsigned repetitionTimes
    );
};
