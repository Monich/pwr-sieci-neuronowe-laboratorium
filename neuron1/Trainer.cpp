#include <iostream>
#include "Trainer.hpp"

#include "Neuron.hpp"
#include "Tester.hpp"

Trainer::Trainer(NewNeuronFactory& neuronFactory,
                 TrainingExitClause &trainingExitClause) :
    neuronFactory(neuronFactory),
    trainingExitClause(trainingExitClause)
{}

u64 Trainer::train(std::vector<NeuronData>& trainingData)
{
    u64 iterations{0};
    std::unique_ptr<Neuron> neuron = neuronFactory.createNeuron();
    do
    {
        iterations++;
        for (const auto &trainingEntry : trainingData)
        {
            neuron->learn(trainingEntry);
            if (trainingExitClause.shouldExit(neuron, trainingData))
            {
                return iterations;
            }
        }
    } while (true);

    return iterations;
}
