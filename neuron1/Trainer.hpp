#pragma once

#include <memory>
#include <vector>

using u64 = unsigned long long;

class NewNeuronFactory;
class TrainingExitClause;
struct NeuronData;

class Trainer
{
public:
    explicit Trainer(NewNeuronFactory& neuronFactory,
                     TrainingExitClause& trainingExitClause);

    u64 train(std::vector<NeuronData>& trainingData);
private:
    NewNeuronFactory &neuronFactory;
    TrainingExitClause &trainingExitClause;
};
