#include "Neuron.hpp"
#include "NewPerceptronFactory.hpp"
#include "Tester.hpp"
#include "StrictExitClause.hpp"
#include "NewAdalineFactory.hpp"

const unsigned REPETITION_TIMES{10000};
const double MEAN_ERROR_ACCEPTED{0.3};

std::vector<NeuronData> TRAINING_DATA_AND{
        {0, 0, 0},
        {1, 0, 0},
        {0, 1, 0},
        {1, 1, 1},
};

std::vector<NeuronData> TRAINING_DATA_OR{
        {0, 0, 0},
        {1, 0, 1},
        {0, 1, 1},
        {1, 1, 1},
};

std::vector<NeuronData> TRAINING_DATA_AND_BIPOLAR{
        {-1, -1, -1},
        {1, -1, -1},
        {-1, 1, -1},
        {1, 1, 1},
};

std::vector<NeuronData> TRAINING_DATA_OR_BIPOLAR{
        {0, 0, -1},
        {1, 0, 1},
        {0, 1, 1},
        {1, 1, 1},
};

std::vector<NeuronData> TRAINING_DATA_OR_ADALINE{
        {-1, -1, -1},
        {1, -1, 1},
        {-1, 1, 1},
        {1, 1, 1},
};


namespace Excersise1
{
    template <typename ExitClause = StrictExitClause>
    void performTest(
            const std::string& testname,
            std::vector<NeuronData>& trainingData,
            std::vector<TestSuite>& testSuites
    )
    {
        ExitClause strictExitClause{};
        Tester::performTest(
                testname,
                testSuites,
                strictExitClause,
                trainingData,
                REPETITION_TIMES
        );
    }

    template <>
    void performTest<MeanErrorExitClause>(
            const std::string& testname,
            std::vector<NeuronData>& trainingData,
            std::vector<TestSuite>& testSuites
    )
    {
        MeanErrorExitClause strictExitClause{MEAN_ERROR_ACCEPTED};
        Tester::performTest(
                testname,
                testSuites,
                strictExitClause,
                trainingData,
                REPETITION_TIMES
        );
    }

    template<typename NeuronFactory, typename ExitClause = StrictExitClause>
    void performMaxMinTest(
            const std::string& testname,
            std::vector<NeuronData>& trainingData,
            ActivationFunction& activationFunction = activationUnipolar)
    {
        auto testSuites = NeuronFactory::createFactories(
                {
                        "min: -1, max: 1",
                        "min: -0.8, max: 0.8",
                        "min: -0.6, max: 0.6",
                        "min: -0.4, max: 0.4",
                        "min: -0.2, max: 0.2",
                        "min: 0, max: 0",
                },
                0.01,
                {
                        -1,
                        -0.8,
                        -0.6,
                        -0.4,
                        -0.2,
                        0,
                },
                {
                        1,
                        0.8,
                        0.6,
                        0.4,
                        0.2,
                        0,
                },
                activationFunction
        );

        performTest<ExitClause>(
                testname,
                trainingData,
                testSuites
        );
    }

    template<typename NeuronFactory = NewPerceptronFactory, typename ExitClause = StrictExitClause>
    void performLearnRateTest(
            const std::string& testname,
            std::vector<NeuronData>& trainingData,
            ActivationFunction& activationFunction = activationUnipolar)
    {
        auto testSuites = NeuronFactory::createFactories(
                {
                        "lr: 0.1",
                        "lr: 0.05",
                        "lr: 0.01",
                        "lr: 0.005",
                        "lr: 0.001",
                },
                {
                        0.1,
                        0.05,
                        0.01,
                        0.005,
                        0.001,
                },
                -5,
                5,
                activationFunction
        );

        performTest<ExitClause>(
                testname,
                trainingData,
                testSuites
        );
    }

    void performFunctionTest(
            const std::string& testname,
            std::vector<NeuronData>& trainingData,
            ActivationFunction &activationFunction)
    {
        std::vector<TestSuite> testSuites;
        testSuites.push_back({
                                     "test",
                                     std::make_unique<NewPerceptronFactory>(
                                             0.01,
                                             -5,
                                             5,
                                             activationFunction
                                     )
                             });

        performTest(
                testname,
                trainingData,
                testSuites
        );
    }

    void performMinMaxTestAnd()
    {
        performMaxMinTest<NewPerceptronFactory>(
                "Perceptron Min-Max Test for AND",
                TRAINING_DATA_AND);
    }

    void performMinMaxTestOr()
    {
        performMaxMinTest<NewPerceptronFactory>("Perceptron Min-Max Test for OR", TRAINING_DATA_OR);
    }

    void performLearnRateTestAnd()
    {
        performLearnRateTest("Perceptron LR Test for AND", TRAINING_DATA_AND);
    }

    void performLearnRateTestOr()
    {
        performLearnRateTest("Perceptron LR Test for OR", TRAINING_DATA_OR);
    }

    void performFunctionTestAnd()
    {
        performFunctionTest("Perceptron Unipolar Test for AND", TRAINING_DATA_AND, activationUnipolar);
        performFunctionTest("Perceptron Bipolar Test for AND", TRAINING_DATA_AND_BIPOLAR, activationBipolar);
    }

    void performFunctionTestOr()
    {
        performFunctionTest("Perceptron Unipolar Test for OR", TRAINING_DATA_OR, activationUnipolar);
        performFunctionTest("Perceptron Bipolar Test for OR", TRAINING_DATA_OR_BIPOLAR, activationBipolar);
    }
}

namespace Excersise2
{
    void performMinMaxTestAnd()
    {
        Excersise1::performMaxMinTest<NewPerceptronFactory>(
                "Perceptron Bipolar Min-Max Test for AND",
                TRAINING_DATA_AND_BIPOLAR,
                activationBipolar);
        
        Excersise1::performMaxMinTest<NewAdalineFactory, MeanErrorExitClause>(
                "Adaline Min-Max Test for AND",
                TRAINING_DATA_AND_BIPOLAR,
                activationBipolarAdaline);
    }

    void performMinMaxTestOr()
    {
        Excersise1::performMaxMinTest<NewPerceptronFactory>(
                "Perceptron bipolar Min-Max Test for OR",
                TRAINING_DATA_OR_BIPOLAR,
                activationBipolar);

        Excersise1::performMaxMinTest<NewAdalineFactory, MeanErrorExitClause>(
                "Adaline Min-Max Test for OR",
                TRAINING_DATA_OR_ADALINE,
                activationBipolarAdaline);
    }

    void performLearnRateTestAnd()
    {
        Excersise1::performLearnRateTest("Perceptron bipolar LR Test for AND", TRAINING_DATA_AND_BIPOLAR, activationBipolar);
        Excersise1::performLearnRateTest<NewAdalineFactory, MeanErrorExitClause>("Adaline LR Test for AND", TRAINING_DATA_AND_BIPOLAR, activationBipolarAdaline);
    }

    void performLearnRateTestOr()
    {
        Excersise1::performLearnRateTest("Perceptron bipolar LR Test for OR", TRAINING_DATA_OR_BIPOLAR, activationBipolar);
        Excersise1::performLearnRateTest<NewAdalineFactory, MeanErrorExitClause>("Adaline LR Test for OR", TRAINING_DATA_OR_ADALINE, activationBipolarAdaline);
    }
}

int main()
{
    // Zad 1-1
    Excersise1::performMinMaxTestAnd();
    Excersise1::performMinMaxTestOr();

    // Zad 1-2
    Excersise1::performLearnRateTestAnd();
    Excersise1::performLearnRateTestOr();

    // Zad 1-3
    Excersise1::performFunctionTestAnd();
    Excersise1::performFunctionTestOr();

    // Zad 2-1
    Excersise2::performMinMaxTestAnd();
    Excersise2::performMinMaxTestOr();

    // Zad 2-2
    Excersise2::performLearnRateTestAnd();
    Excersise2::performLearnRateTestOr();

    return 0;
}
