#include <iostream>
#include "ConvolutionalNeuralNetwork.hpp"
#include "Trainer.hpp"

ConvolutionalNeuralNetwork::ConvolutionalNeuralNetwork(unsigned filterSize, unsigned filterStep, unsigned filterCount) :
    endNetwork(13*13*filterCount, 50, 10, 0.2)
{
    for (unsigned i = 0; i < filterCount; ++i)
    {
        filters.push_back(Filter(filterSize, filterStep));
    }
}

ConvolutionalNeuralNetwork::ConvolutionalNeuralNetwork(const TrainingParameters& params) :
    endNetwork(13*13*params.filterCount, params.hiddenNeurons, params.outputNeurons, params.initialWeightValue)
{
    for (unsigned i = 0; i < params.filterCount; ++i)
    {
        filters.push_back(Filter(params.filterSize, params.filterStep));
    }
}

unsigned ConvolutionalNeuralNetwork::calculate(const NeuralNetworkInput &input) const
{
    NeuralNetworkInput endNetworkInput;
    convolute(input, endNetworkInput);

    return endNetwork.calculate(endNetworkInput);
}

void ConvolutionalNeuralNetwork::learn(const NeuronData &data, bool, bool, bool, unsigned iteration)
{
    NeuralNetworkInput endNetworkInput;
    convolute(data.x, endNetworkInput);
    endNetwork.learn({endNetworkInput, data.y}, false, false, false, iteration);

    NeuralNetworkInput expectedMaxPoolingValues = endNetwork.getHiddenLayerExpectedInput({endNetworkInput, data.y});
    auto expectedMaxPoolingValuesIterator = expectedMaxPoolingValues.begin();

    for (auto &filter : filters)
    {
        NeuralNetworkInput filterOutput{filter.createFeatureMap(data.x)};
//        std::cout << "Size of feature map = " << filterOutput.size();
        NeuralNetworkInput filterGradient;
        for (unsigned i = 0; i < filterOutput.size(); ++i)
        {
            filterGradient.push_back(0);
        }

        // max pooling gradients
        for (int w = 0; w < 13; ++w)
        {
            unsigned w2 = w*2;
            unsigned w21 = w2+1;
            for (int h = 0; h < 13; ++h)
            {
                unsigned h2 = h*2;
                unsigned h21 = h2+1;
                NeuralNetworkInput filterOutputs{
                       filterOutput.getPixelValue(h2, w2),
                       filterOutput.getPixelValue(h21, w2),
                       filterOutput.getPixelValue(h2, w21),
                       filterOutput.getPixelValue(h21, w21),
                 };

                unsigned maxElement = getMaxElement(filterOutputs);
                NeuralNetworkInput gradientChanges{0,0,0,0};
                gradientChanges[maxElement] = *expectedMaxPoolingValuesIterator;
                filterGradient.addPixelValue(h2, w2, gradientChanges[0]);
                filterGradient.addPixelValue(h21, w2, gradientChanges[1]);
                filterGradient.addPixelValue(h2, w21, gradientChanges[2]);
                filterGradient.addPixelValue(h21, w21, gradientChanges[3]);

                expectedMaxPoolingValuesIterator++;
            }
        }

        filter.learn(data.x, filterGradient);
    }
}

void ConvolutionalNeuralNetwork::convolute(const NeuralNetworkInput &input, NeuralNetworkInput &endNetworkInput) const
{
    for (const auto &filter : filters)
    {
        NeuralNetworkInput filterOutput{filter.createFeatureMap(input)};

        // max pooling
        for (int w = 0; w < 13; ++w)
        {
            unsigned w2 = w*2;
            unsigned w21 = w2+1;
            for (int h = 0; h < 13; ++h)
            {
                unsigned h2 = h*2;
                unsigned h21 = h2+1;
                endNetworkInput.push_back(std::max({
                                                           filterOutput.getPixelValue(h2, w2),
                                                           filterOutput.getPixelValue(h21, w2),
                                                           filterOutput.getPixelValue(h2, w21),
                                                           filterOutput.getPixelValue(h21, w21),
                                                   }));
            }
        }
    }
}

void ConvolutionalNeuralNetwork::completeBatch(const LearningRate &learningRate, double)
{
    endNetwork.completeBatch(learningRate, 1);
    for (auto &l_filter : filters)
    {
        l_filter.completeBatch(learningRate);
    }
}

unsigned int ConvolutionalNeuralNetwork::getMaxElement(const NeuralNetworkInput& p_input)
{
    unsigned maxElementIndex = 0;
    double maxElement = p_input[0];
    for (int i = 1; i < p_input.size(); ++i)
    {
        if(maxElement < p_input[i])
        {
            maxElement = p_input[i];
            maxElementIndex = i;
        }
    }

    return maxElementIndex;
}
