#pragma once

#include "Filter.hpp"

class Filter;

class ConvolutionalNeuralNetwork
{
public:
    ConvolutionalNeuralNetwork(unsigned filterSize, unsigned filterStep, unsigned filterCount);
    ConvolutionalNeuralNetwork(const TrainingParameters& params);
    unsigned calculate(const NeuralNetworkInput& input) const;
    void learn(const NeuronData& data, bool, bool, bool, unsigned);
    void completeBatch(const LearningRate& learningRate, double);

private:
    std::vector<Filter> filters;
    NeuralNetwork endNetwork;

    void convolute(const NeuralNetworkInput& input, NeuralNetworkInput& endNetworkInput) const;
    unsigned int getMaxElement(const NeuralNetworkInput& p_input);
};
