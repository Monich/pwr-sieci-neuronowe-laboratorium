#include "DataSet.hpp"

#include "cmake_defines.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

void loadIntoDataVector(
        std::vector<NeuronData>& set,
        unsigned entries,
        std::istream& dataFile,
        std::istream& answerFile
    )
{
    std::string line;

    for (unsigned i = 0; i < entries; ++i)
    {
        NeuronData data{};

        if(!std::getline(dataFile, line))
            throw std::runtime_error("No more data in file");

        std::stringstream lineStream(line);
        std::string cell;
        std::vector<double> temporaryVector;
        while(std::getline(lineStream,cell,';'))
        {
            double value = std::stod(cell);
            temporaryVector.push_back(value);
        }

        data.x = std::move(temporaryVector);
        answerFile >> data.y;
        std::getline(answerFile, line);

		if (!answerFile)
			throw std::runtime_error("Answer file error");
        if (!dataFile)
            throw std::runtime_error("Data file error");

        set.push_back(data);
    }
}

DataSet loadDataSet()
{
    DataSet dataSet{};

    std::ifstream dataFile(DATASET_LOC);
    std::ifstream answerFile(ANSWER_LOC);

	try
	{
		std::cout << "Loading training data" << std::endl;
		loadIntoDataVector(dataSet.trainingData, 6000, dataFile, answerFile);
		std::cout << "Loading validation data" << std::endl; 
		loadIntoDataVector(dataSet.validationData, 1000, dataFile, answerFile);
		std::cout << "Loading testing data" << std::endl; 
		loadIntoDataVector(dataSet.testData, 0, dataFile, answerFile);
	}
	catch (const std::runtime_error& error)
    {
	    std::cout << "Error: " << error.what() << std::endl;
    }

    return dataSet;
}
