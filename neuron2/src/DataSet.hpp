#pragma once

#include "NeuralNetwork.hpp"

struct NeuronData
{
    NeuralNetworkInput x;
	unsigned y;
};

struct DataSet
{
    std::vector<NeuronData> trainingData;
    std::vector<NeuronData> validationData;
    std::vector<NeuronData> testData;
};

DataSet loadDataSet();
