#include <iostream>
#include "Filter.hpp"

#include "Neuron.hpp"
#include "LearningRateCalculator.hpp"

Filter::Filter(unsigned filterSize, unsigned filterStep) : filterStep(filterStep), filterSize(filterSize)
{
    std::uniform_real_distribution<double> unif(-0.2, 0.2);
    for (unsigned i = 0; i < filterSize * filterSize; ++i)
    {
        weights.push_back(unif(randomEngine));
        weightsChange.push_back(0);
    }
}

NeuralNetworkInput Filter::createFeatureMap(const NeuralNetworkInput &input) const
{
    NeuralNetworkInput result;

    for (unsigned w = 0; w < 26; w += filterStep)
    {
        for (unsigned h = 0; h < 26; h += filterStep)
        {
            double pixelResult = createOutputForPixel(input, h+1, w+1);
            if(pixelResult < 0)
                pixelResult = 0;
            result.push_back(pixelResult);
        }
    }

    return std::move(result);
}

double Filter::createOutputForPixel(const NeuralNetworkInput &input, unsigned centerH, unsigned centerW) const
{
    double result = 0;
    unsigned filterIndex = 0;

    for (unsigned w = 0; w < filterSize; ++w)
    {
        for (unsigned h = 0; h < filterSize; ++h)
        {
            result += input.getPixelValue(centerH+h, centerW+w) * weights[filterIndex++];
        }
    }

    return result;
}

void Filter::learn(const NeuralNetworkInput &input, const NeuralNetworkInput& gradient)
{
    auto gradientIterator = gradient.begin();
//    std::cout << " Size of input = " << input.size();
//    std::cout << " Size of gradient = " << gradient.size();

    for (unsigned w = 0; w < 26; w += filterStep)
    {
        for (unsigned h = 0; h < 26; h += filterStep)
        {
            if(*gradientIterator != 0)
            {
                learnForPixel(input, h+1, w+1, *gradientIterator);
            }
            gradientIterator++;
        }
    }
}

void Filter::learnForPixel(const NeuralNetworkInput &input, unsigned centerH, unsigned centerW, double gradient)
{
    unsigned filterIndex = 0;

    for (unsigned w = 0; w < filterSize; ++w)
    {
        for (unsigned h = 0; h < filterSize; ++h)
        {
            weightsChange[filterIndex++] += input.getPixelValue(centerH+h, centerW+w) * gradient;
        }
    }
}

void Filter::completeBatch(const LearningRate& p_learningRate)
{
    for (int i = 0; i < weights.size(); ++i)
    {
        weights[i] += p_learningRate.rate * weightsChange[i];
        weightsChange[i] = 0;
    }
}
