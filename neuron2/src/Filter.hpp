#pragma once

#include "NeuralNetwork.hpp"

class Filter
{
public:
    Filter(unsigned filterSize, unsigned filterStep);
    NeuralNetworkInput createFeatureMap(const NeuralNetworkInput& input) const;
    void learn(const NeuralNetworkInput& input, const NeuralNetworkInput& gradient);
    void completeBatch(const LearningRate& p_learningRate);

private:
    std::vector<double> weights;
    std::vector<double> weightsChange;
    unsigned filterStep;
    unsigned filterSize;

    double createOutputForPixel(const NeuralNetworkInput& input, unsigned centerH, unsigned centerW) const;
    void learnForPixel(const NeuralNetworkInput& input, unsigned centerH, unsigned centerW, double gradient);
};
