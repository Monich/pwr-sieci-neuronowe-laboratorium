#include <iostream>
#include "LearningRateCalculator.hpp"

bool LearningRateCalculator::isAdaGradActivated()
{
    return false;
}

bool LearningRateCalculator::isAdaDeltaActivated()
{
    return false;
}

bool LearningRateCalculator::isAdamActivated()
{
    return false;
}

ConstantLearningRateCalculator::ConstantLearningRateCalculator(double learningRate) : learningRate(learningRate) {}

LearningRate ConstantLearningRateCalculator::calculateLearningRate(double currentError)
{
    return { learningRate, 0.f };
}

AdaptiveLearningRateCalculator::AdaptiveLearningRateCalculator(double beginRate, double errorTolerance,
                                                               double decreaseFactor, double increaseFactor) :
        lastIterationLearningRate(beginRate),
        errorTolerance(errorTolerance),
        decreaseFactor(decreaseFactor),
        increaseFactor(increaseFactor)
{}

LearningRate AdaptiveLearningRateCalculator::calculateLearningRate(double currentError)
{
    if(lastIterationError != 0)
    {
//        std::cout << "lastItError = " << lastIterationError << ", currentError = " << currentError << std::endl;
        if(lastIterationError > errorTolerance * currentError)
        {
//            std::cout << "Error out of range" << std::endl;
            lastIterationLearningRate *= decreaseFactor;
        }
        else
        {
//            std::cout << "Error in range" << std::endl;
            lastIterationLearningRate *= increaseFactor;
        }
    }

    lastIterationError = currentError;
//    std::cout << "Current learning rate = " << lastIterationLearningRate << std::endl;
    return { lastIterationLearningRate, 0.f };
}

MomentumBasedLearningRateCalculator::MomentumBasedLearningRateCalculator(double learningRate, double momentumRate)
        : learningRate(learningRate), momentumRate(momentumRate) {}

LearningRate MomentumBasedLearningRateCalculator::calculateLearningRate(double currentError)
{
    return {learningRate, momentumRate};
}

AdaGradCalculator::AdaGradCalculator(double learningRate) : ConstantLearningRateCalculator(learningRate),
                                                            wasOneTurnSkipped(false)
{}

bool AdaGradCalculator::isAdaGradActivated()
{
    return true;
}

bool AdaDeltaCalculator::isAdaDeltaActivated()
{
    return true;
}

LearningRate AdaDeltaCalculator::calculateLearningRate(double currentError)
{
    return {1, 0};
}

AdamCalculator::AdamCalculator() : ConstantLearningRateCalculator(0.0001)
{}

bool AdamCalculator::isAdamActivated()
{
    return true;
}
