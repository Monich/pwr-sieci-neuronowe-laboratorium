#pragma once

struct LearningRate
{
    double rate;
    double momentumRate;
};

class LearningRateCalculator
{
public:
    virtual ~LearningRateCalculator() = default;
    virtual LearningRate calculateLearningRate(double currentError) = 0;
    virtual bool isAdaGradActivated();
    virtual bool isAdaDeltaActivated();
    virtual bool isAdamActivated();
};

class ConstantLearningRateCalculator : public LearningRateCalculator
{
public:
    explicit ConstantLearningRateCalculator(double learningRate);
    LearningRate calculateLearningRate(double currentError) override;

private:
    double learningRate;
};

class AdaptiveLearningRateCalculator : public LearningRateCalculator
{
public:
    explicit AdaptiveLearningRateCalculator(double beginRate = 0.1,
                                            double errorTolerance = 1.15,
                                            double decreaseFactor = 0.7,
                                            double increaseFactor = 1.05);
    LearningRate calculateLearningRate(double currentError) override;

private:
    double lastIterationLearningRate;
    double lastIterationError{0};
    double errorTolerance;
    double decreaseFactor;
    double increaseFactor;
};

class MomentumBasedLearningRateCalculator : public LearningRateCalculator
{
public:
    MomentumBasedLearningRateCalculator(double learningRate, double momentumRate);

    LearningRate calculateLearningRate(double currentError) override;

private:
    double learningRate;
    double momentumRate;
};

class AdaGradCalculator : public ConstantLearningRateCalculator
{
public:
    explicit AdaGradCalculator(double learningRate);
    bool isAdaGradActivated() override;

private:
    bool wasOneTurnSkipped;
};


class AdaDeltaCalculator : public LearningRateCalculator
{
public:
    LearningRate calculateLearningRate(double currentError) override;

    bool isAdaDeltaActivated() override;
};

class AdamCalculator : public ConstantLearningRateCalculator
{
public:
    AdamCalculator();
    bool isAdamActivated() override;
};
