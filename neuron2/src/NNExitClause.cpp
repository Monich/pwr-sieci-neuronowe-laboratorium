#include <iostream>
#include "NNExitClause.hpp"

#define ITERATIONS 25

NNExitClause::NNExitClause(unsigned iterationsExpected) :
    ticksExpected(iterationsExpected * (ITERATIONS + 1))
{
    clear();
}

bool NNExitClause::shouldExit(double validationResult, unsigned iteration, unsigned id)
{
    ticks++;
    double percentDone = static_cast<double>(ticks * 100) / ticksExpected;
    std::lock_guard<std::mutex> lockGuard{mutex};

    std::cout << "[" << percentDone << "%][" << id << "] Iteration " << iteration << ", result = " << validationResult << "%" << std::endl;
    results[iteration] += validationResult;

    return iteration == ITERATIONS;
}

const std::vector<double> &NNExitClause::getResults() const
{
    return results;
}

void NNExitClause::finishCalculations(unsigned timesRun)
{
    for (double & result : results)
    {
        result /= timesRun;
    }
}

void NNExitClause::clear()
{
    results.clear();
    results.reserve(ITERATIONS + 1);
    for (int i = 0; i < ITERATIONS + 1; ++i)
    {
        results.push_back(0);
    }
    ticks = 0;
}
