#pragma once

#include <mutex>
#include <vector>

class NNExitClause
{
public:
    NNExitClause(unsigned iterationsExpected);

    bool shouldExit(double validationResult, unsigned iteration, unsigned id);
    void finishCalculations(unsigned timesRun);

    const std::vector<double> &getResults() const;

    void clear();

private:
    std::vector<double> results;
    std::mutex mutex;
    const unsigned ticksExpected;
    unsigned ticks;
};
