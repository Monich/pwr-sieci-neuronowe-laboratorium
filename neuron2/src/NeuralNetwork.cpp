#include "NeuralNetwork.hpp"

#include "DataSet.hpp"
#include "Trainer.hpp"

namespace
{
    NeuralNetworkInput getLayerOutput(const std::vector<Neuron> &layer, const NeuralNetworkInput& input)
    {
        NeuralNetworkInput output;

        for (const auto &item : layer)
        {
            output.push_back(item.calculate(input));
        }

        return output;
    }
}

NeuralNetwork::NeuralNetwork(
        unsigned inputSize,
        unsigned hiddenLayerNeurons,
        unsigned outputLayerNeurons,
        double initialWeightValue)
{
    init(inputSize, hiddenLayerNeurons, outputLayerNeurons, initialWeightValue);
}

NeuralNetwork::NeuralNetwork(const TrainingParameters &parameters)
{
    init(parameters.inputNeurons,
         parameters.hiddenNeurons,
         parameters.outputNeurons,
         parameters.initialWeightValue);
}


void NeuralNetwork::init(unsigned inputSize, unsigned hiddenLayerNeurons, unsigned outputLayerNeurons,
                         double initialWeightValue)
{
    for (unsigned i = 0; i < hiddenLayerNeurons; ++i)
    {
        hiddenLayer.push_back({inputSize, initialWeightValue});
    }

    for (unsigned i = 0; i < outputLayerNeurons; ++i)
    {
        outputLayer.push_back({hiddenLayerNeurons, initialWeightValue});
    }
}

unsigned NeuralNetwork::calculate(const NeuralNetworkInput &input) const
{
    return returnFunction(getOutputLayerActivationLevel(getHiddenLayerActivationLevel(input)));
}

void NeuralNetwork::learn(const NeuronData& data, bool activateAdaGrad, bool activateAdaDelta, bool activateAdam, unsigned iteration)
{
    NeuralNetworkInput hiddenLayerOutput = getHiddenLayerActivationLevel(data.x);

    for (unsigned i = 0; i < outputLayer.size(); ++i)
    {
        double expectedOutput = data.y == i ? 1 : -1;
        outputLayer[i].learn(hiddenLayerOutput, expectedOutput, activateAdaGrad, activateAdaDelta, activateAdam, iteration);
    }

    for (unsigned i = 0; i < hiddenLayer.size(); ++i)
    {
        double expectedHiddenValue{0};
        for (unsigned j = 0; j < outputLayer.size(); ++j)
        {
            double expectedOutput = data.y == j ? 1 : -1;
            expectedHiddenValue += outputLayer[j].getWeight(i) * outputLayer[j].getError(hiddenLayerOutput, expectedOutput);
        }
        hiddenLayer[i].learn(data.x, expectedHiddenValue, activateAdaGrad, activateAdaDelta, activateAdam, iteration);
    }
}

NeuralNetworkInput NeuralNetwork::getHiddenLayerActivationLevel(const NeuralNetworkInput &input) const
{
    return getLayerOutput(hiddenLayer, input);
}

NeuralNetworkInput NeuralNetwork::getOutputLayerActivationLevel(const NeuralNetworkInput &hiddenLayerOutput) const
{
    return getLayerOutput(outputLayer, hiddenLayerOutput);
}

unsigned NeuralNetwork::returnFunction(const NeuralNetworkInput &outputMatrix) const
{
    double maxValue = outputMatrix[0];
    unsigned maxIndex = 0;
    for (unsigned i = 1; i < outputMatrix.size(); ++i)
    {
        if(outputMatrix[i] > maxValue)
        {
            maxIndex = i;
            maxValue = outputMatrix[i];
        }
    }

    return maxIndex;
}

void NeuralNetwork::completeBatch(const LearningRate &learningRate, double batchSize)
{
    for (auto &neuron : hiddenLayer)
    {
        neuron.completeBatch(learningRate, batchSize);
    }
    for (auto &neuron : outputLayer)
    {
        neuron.completeBatch(learningRate, batchSize);
    }
}

NeuralNetworkInput NeuralNetwork::getHiddenLayerExpectedInput(const NeuronData& data) const
{
    NeuralNetworkInput hiddenLayerOutput = getHiddenLayerActivationLevel(data.x);

    NeuralNetworkInput hiddenLayerExpectedOutput{};
    for (unsigned i = 0; i < hiddenLayer.size(); ++i)
    {
        double expectedHiddenValue{0};
        for (unsigned j = 0; j < outputLayer.size(); ++j)
        {
            double expectedOutput = data.y == j ? 1 : -1;
            expectedHiddenValue += outputLayer[j].getWeight(i) * outputLayer[j].getError(hiddenLayerOutput, expectedOutput);
        }
        hiddenLayerExpectedOutput.push_back(expectedHiddenValue);
    }

    NeuralNetworkInput hiddenLayerExpectedInput{};
    for (unsigned i = 0; i < data.x.size(); ++i)
    {
        double expectedValue{0};
        for (unsigned j = 0; j < hiddenLayer.size(); ++j)
        {
            double expectedOutput = data.y == j ? 1 : -1;
            expectedValue += hiddenLayer[j].getWeight(i) * hiddenLayer[j].getError(data.x, expectedOutput);
        }
        hiddenLayerExpectedInput.push_back(expectedValue);
    }

    return hiddenLayerExpectedInput;
}

double NeuralNetworkInput::getPixelValue(unsigned h, unsigned w) const
{
    if(oneDimensionSize == 0)
        oneDimensionSize = std::sqrt(size());

    if( w >= oneDimensionSize || h >= oneDimensionSize )
        return 0;
    else
        return at(h * oneDimensionSize + w);
}

NeuralNetworkInput &NeuralNetworkInput::operator=(std::vector<double> && moveRes)
{
    std::vector<double>::operator=(std::move(moveRes));
    return *this;
}

NeuralNetworkInput::NeuralNetworkInput(std::initializer_list<double> list) : std::vector<double>(list)
{}

void NeuralNetworkInput::addPixelValue(unsigned h, unsigned w, double value)
{
    if(oneDimensionSize == 0)
        oneDimensionSize = std::sqrt(size());

    if( w < oneDimensionSize && h < oneDimensionSize )
        at(h * oneDimensionSize + w) += value;
}
