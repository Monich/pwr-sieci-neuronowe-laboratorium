#pragma once

#include "Neuron.hpp"

struct NeuronData;
struct TrainingParameters;

struct NeuralNetworkInput : public std::vector<double>
{
    NeuralNetworkInput() = default;
    NeuralNetworkInput(std::initializer_list<double> list);
    NeuralNetworkInput & NeuralNetworkInput::operator=(std::vector<double>&&);
    double getPixelValue(unsigned h, unsigned w) const;
    void addPixelValue(unsigned h, unsigned w, double value);
private:
    mutable unsigned oneDimensionSize = 0;
};

class NeuralNetwork
{
public:
    NeuralNetwork(
            unsigned inputSize,
            unsigned hiddenLayerNeurons,
            unsigned outputLayerNeurons,
            double initialWeightValue
        );
    NeuralNetwork(const TrainingParameters& parameters);

    unsigned calculate(const NeuralNetworkInput& input) const;
    void learn(const NeuronData& data, bool activateAdaGrad, bool activateAdaDelta, bool activateAdam, unsigned iteration);
    void completeBatch(const LearningRate& learningRate, double batchSize);

    NeuralNetworkInput getHiddenLayerExpectedInput(const NeuronData& data) const;

private:
    NeuralNetworkInput getHiddenLayerActivationLevel(const NeuralNetworkInput &input) const;
    NeuralNetworkInput getOutputLayerActivationLevel(const NeuralNetworkInput &hiddenLayerOutput) const;
    unsigned returnFunction(const NeuralNetworkInput& outputLayerOutput) const;

    void init(
            unsigned inputSize,
            unsigned hiddenLayerNeurons,
            unsigned outputLayerNeurons,
            double initialWeightValue);

    std::vector<Neuron> hiddenLayer;
    std::vector<Neuron> outputLayer;
};
