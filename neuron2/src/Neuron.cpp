#include "Neuron.hpp"
#include "LearningRateCalculator.hpp"

#include <chrono>
#include <iostream>

namespace
{

    constexpr auto DEFAULT_PARAMETER = 1;

    // tangens hiperboliczny
    double activationFunction(double input, double param = DEFAULT_PARAMETER)
    {
        return 2 / (1 + std::exp(-param * input)) - 1;
    }

    double activationFunctionDerivative(double input, double param = DEFAULT_PARAMETER)
    {
        double expotential = std::exp(-param * input);
        double returnVal = 2 * param * expotential / std::pow(1 + expotential, 2);

        if (isnan(returnVal))
        {
            std::cout << "I've got a NaN!" << std::endl;
            return 0;
        }

        return returnVal;
    }

}

std::default_random_engine randomEngine{static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count())};

Neuron::Neuron(unsigned inputCount, double initialValue)
{
    std::uniform_real_distribution<double> unif(-initialValue, initialValue);
    weights.reserve(inputCount);
    currentWeightsChange.reserve(inputCount);
    weightsChangeInLastIteration.reserve(inputCount);
    sumOfGradientSquared.reserve(inputCount);
    RMSforGradient.reserve(inputCount);
    RMSforWeights.reserve(inputCount);
    m.reserve(inputCount);
    v.reserve(inputCount);

    for (unsigned i = 0; i < inputCount; ++i)
    {
        weights.push_back(unif(randomEngine));
        currentWeightsChange.push_back(0);
        weightsChangeInLastIteration.push_back(0);
        sumOfGradientSquared.push_back(0);
        RMSforGradient.push_back(0);
        RMSforWeights.push_back(0);
        m.push_back(0);
        v.push_back(0);
    }
    bias = unif(randomEngine);
    currentBiasChange = 0;
    lastBiasChange = 0;
    sumOfGradientSquaredForBias = 0;
    RMSforGradientBias = 0;
    RMSforWeightsBias = 0;
    mForBias = 0;
    vForBias = 0;
}

double Neuron::calculate(const std::vector<double>& input) const
{
    return activationFunction(calculateWithoutActivationFunction(input));
}

void Neuron::learn(const std::vector<double> &input, double answer, bool activateAdaGrad, bool activateAdaDelta, bool activateAdam, unsigned iteration)
{
    double factor = getError(input, answer);

    for (unsigned i = 0; i < input.size(); ++i)
    {
        auto gradient = factor * input[i];

        double weightChange;
        if(activateAdaGrad)
        {
            weightChange = gradient / (std::sqrt(sumOfGradientSquared[i] + 0.1));
        }
        else if(activateAdaDelta)
        {
            RMSforGradient[i] = 0.9 * RMSforGradient[i] + 0.1 * std::pow(gradient, 2);
            weightChange = gradient * std::sqrt(RMSforWeights[i] + 0.001) / std::sqrt(RMSforGradient[i] + 0.001);
            RMSforWeights[i] = 0.9 * RMSforWeights[i] + 0.1 * std::pow(weightChange, 2);
        }
        else if(activateAdam)
        {
            m[i] = 0.9 * m[i] + 0.1 * gradient;
            v[i] = 0.999 * v[i] + 0.001 * std::pow(gradient, 2);
            double mt = m[i] / (1 - std::pow(0.9, iteration));
            double vt = v[i] / (1 - std::pow(0.999, iteration));
            weightChange = mt / (std::sqrt(vt) + 0.001);
        }
        else
        {
            weightChange = gradient;
        }

        if (isnan(weightChange))
        {
            std::cout << "I've got a NaN!!" << std::endl;
            return;
        }

        currentWeightsChange[i] += weightChange;
        sumOfGradientSquared[i] += std::pow(gradient, 2);
    }

    double biasChange;
    if(activateAdaGrad)
    {
        biasChange = factor / (std::sqrt(sumOfGradientSquaredForBias + 0.1));
    }
    else if(activateAdaDelta)
    {
        RMSforGradientBias = 0.9 * RMSforGradientBias + 0.1 * std::pow(factor, 2);
        biasChange = std::sqrt(RMSforWeightsBias + 0.001) / std::sqrt(RMSforGradientBias + 0.001) * factor;
        RMSforWeightsBias = 0.9 * RMSforWeightsBias + 0.1 * std::pow(currentBiasChange, 2);
    }
    else if (activateAdam)
    {
        mForBias = 0.9 * mForBias + 0.1 * factor;
        vForBias = 0.999 * vForBias + 0.001 * std::pow(factor, 2);
        double mt = mForBias / (1 - std::pow(0.9, iteration));
        double vt = vForBias / (1 - std::pow(0.999, iteration));
        biasChange = mt / (std::sqrt(vt) + 0.001);
    }
    else
    {
        biasChange = factor;
    }
    if (isnan(biasChange))
    {
        std::cout << "I've got a NaN!!" << std::endl;
        return;
    }
    currentBiasChange += biasChange;
    sumOfGradientSquaredForBias += std::pow(factor, 2);
}

double Neuron::calculateWithoutActivationFunction(const std::vector<double> &input) const
{
    double result = 0;

    for (unsigned i = 0; i < weights.size() - 1; ++i)
    {
        result += input[i] * weights[i];
    }

    result += bias;

    return result;
}

double Neuron::getWeight(unsigned i) const
{
    return weights[i];
}

double Neuron::getError(const std::vector<double> &input, double answer) const
{
    double outputWoActivation = calculateWithoutActivationFunction(input);
    double output = activationFunction(outputWoActivation);
    double result = answer - output;
    double pow = std::pow(result, 2);
    double derivative = activationFunctionDerivative(outputWoActivation);

    double returnVal = derivative * pow * result;

    if (isnan(returnVal))
    {
        std::cout << "I've got a NaN!!" << std::endl;
        return 0;
    }

    return returnVal;
}

void Neuron::completeBatch(const LearningRate &learningRate, double batchSize)
{
    for (unsigned i = 0; i < weights.size(); ++i)
    {
        auto weightChange = learningRate.rate * (currentWeightsChange[i] / batchSize + learningRate.momentumRate * weightsChangeInLastIteration[i]);
        weights[i] += weightChange;
        weightsChangeInLastIteration[i] = weightChange;
        currentWeightsChange[i] = 0;
    }

    auto biasChange = learningRate.rate * (currentBiasChange / batchSize + learningRate.momentumRate * lastBiasChange);
    bias += biasChange;
    lastBiasChange = biasChange;
    currentBiasChange = 0;
}
