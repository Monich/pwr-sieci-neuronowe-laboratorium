#pragma once

#include <vector>
#include <random>

struct LearningRate;

extern std::default_random_engine randomEngine;

class Neuron
{
public:
    Neuron(unsigned inputCount, double initialValue);
    double calculate(const std::vector<double>& input) const;

    void completeBatch(const LearningRate& learningRate, double batchSize);
    void learn(const std::vector<double>& input, double correctAnswer, bool activateAdaGrad, bool activateAdaDelta, bool activateAdam, unsigned iteration);

    double getWeight(unsigned i) const;
    double getError(const std::vector<double> &input, double answer) const;

private:
    double calculateWithoutActivationFunction(const std::vector<double>& input) const;

    std::vector<double> weights;
    double bias;
    std::vector<double> currentWeightsChange;
    double currentBiasChange;
    std::vector<double> weightsChangeInLastIteration;
    double lastBiasChange;
    // AdaGrad
    std::vector<double> sumOfGradientSquared;
    double sumOfGradientSquaredForBias;
    // AdaDelta
    std::vector<double> RMSforGradient;
    std::vector<double> RMSforWeights;
    double RMSforGradientBias;
    double RMSforWeightsBias;
    // Adam
    std::vector<double> m;
    std::vector<double> v;
    double mForBias;
    double vForBias;
};
