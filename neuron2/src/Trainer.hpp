#pragma once

#include "DataSet.hpp"
#include "NNExitClause.hpp"
#include "LearningRateCalculator.hpp"

#include <memory>
#include <vector>

using u64 = unsigned long long;

struct TrainingParameters
{
    NNExitClause& exitClause;
    LearningRateCalculator& learningRate;
    unsigned batchSize;
    double initialWeightValue;
    unsigned hiddenNeurons;
    unsigned filterCount = 0;
    unsigned filterSize = 0;
    unsigned filterStep = 0;
    unsigned inputNeurons = 784;
    unsigned outputNeurons = 10;
};

template<typename Network>
class Trainer
{
public:
    u64 train(const DataSet& data, TrainingParameters parameters, unsigned id)
    {
        u64 iterations{0};
        double percentCorrect, lastePercentCorrect;
        Network network{
                parameters
        };

        percentCorrect = getValidationDataResults(network, data.validationData);
        while (!parameters.exitClause.shouldExit(percentCorrect, iterations, id))
        {
            iterations++;

            auto learningRate = parameters.learningRate.calculateLearningRate(percentCorrect);
            auto activateAdaGrad = parameters.learningRate.isAdaGradActivated();
            auto activateAdaDelta = parameters.learningRate.isAdaDeltaActivated();
            auto activateAdam = parameters.learningRate.isAdamActivated();
            unsigned entryCount = 0;

            for (const auto &trainingEntry : data.trainingData)
            {
                entryCount++;
                network.learn(trainingEntry, activateAdaGrad, activateAdaDelta, activateAdam, iterations);

                if(entryCount % parameters.batchSize == 0)
                {
//                std::cout << "entry " << entryCount << ", performing batch completion" << std::endl;
                    network.completeBatch(learningRate, parameters.batchSize);
                }
            }
            percentCorrect = getValidationDataResults(network, data.validationData);
        }

        return iterations;
    }

    [[nodiscard]] double getValidationDataResults(const Network& network, const std::vector<NeuronData>& data) const
    {
        int correctAnswers{0};
        for (const auto &datum : data)
        {
            unsigned answer = network.calculate(datum.x);
            if (answer == datum.y)
                correctAnswers++;
        }

        return static_cast<double>(correctAnswers) * 100 / data.size();
    }
};
