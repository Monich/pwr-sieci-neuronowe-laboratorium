#include <iostream>
#include <fstream>
#include "DataSet.hpp"
#include "Trainer.hpp"
#include "NNExitClause.hpp"
#include "ConvolutionalNeuralNetwork.hpp"

#define ITERATIONS 10

void trainerThread(const DataSet& dataSet, TrainingParameters& parameters, unsigned id)
{
    Trainer<ConvolutionalNeuralNetwork>{}.train(dataSet, parameters, id);
    id+=5;
    Trainer<ConvolutionalNeuralNetwork>{}.train(dataSet, parameters, id);
}

void mainCalculationGuard(const DataSet& dataSet, TrainingParameters& parameters, const std::string& pathToOutput)
{
    constexpr unsigned THREADS = 5;
    std::thread threads[THREADS];
    for (int i = 0; i < THREADS; ++i)
    {
        std::cout << "Performing run " << i+1 << "/" << ITERATIONS << std::endl;
        threads[i] = std::thread(trainerThread, dataSet, parameters, i);
    }
    for (int j = 0; j < THREADS; ++j)
    {
        threads[j].join();
    }

    std::cout << "Storing results...";
    parameters.exitClause.finishCalculations(ITERATIONS);

    std::ofstream output(pathToOutput);
    for (const auto &result : parameters.exitClause.getResults())
    {
        output << result << std::endl;
    }

    parameters.exitClause.clear();
}

int main()
{
    DataSet dataSet = loadDataSet();
//    ConvolutionalNeuralNetwork cnn(3, 1, 8);
//
//    std::cout << cnn.calculate(dataSet.trainingData[0].x);


    ConstantLearningRateCalculator learningRateCalculator{0.01};

    NNExitClause exitClause{ITERATIONS};
    TrainingParameters parameters
    {
            exitClause,
            learningRateCalculator,
            1,
            0.2,
            100,
            8,
            3,
            1,
    };

    mainCalculationGuard(dataSet, parameters, "output_const_01.txt");
//    trainerThread(dataSet, parameters, 0);

    return 0;
}